import React, { useEffect, useState } from "react";
import { getDataList, getSearchData } from "./listProvider";
import { DebounceInput } from "react-debounce-input";

import logo from "../../Star_Wars_logo-1.png";
import "./style.css";

const List = (props) => {
  const [data, setData] = useState(null);

  const fetchData = async (url) => {
    const result = await getDataList(url);
    setData(result);
  };

  useEffect(() => {
    fetchData();
  }, []);
  if (!data) return null;

  const { results, next } = data;

  const renderCharacter = ({ url, name }, i) => {
    return (
      <tr key={i}>
        <td onClick={() => showCharacter(url)}>{name}</td>
      </tr>
    );
  };

  const showCharacter = (url) => {
    props.history.push("/character", url);
  };

  const search = async (value, key) => {
    if (value) {
      const characters = await getSearchData(value, key);
      setData({ results: characters });
    } else {
      fetchData();
    }
  };

  const nextPage = (url) => {
    fetchData(url);
  };

  const renderCharactersList = (list = []) =>
    list.map((el, i) => renderCharacter(el, i));

  return (
    <>
      <img height="100px" src={logo} alt="star wars logo"></img>
      <div className="buttonWrapper">
        <DebounceInput
          className="textInput"
          minLength={3}
          debounceTimeout={500}
          onChange={(event) => search(event.target.value)}
          placeholder="Search by Film"
        />
      </div>
      <table className="container">
        <thead>
          <tr>
            <th>
              <h1>Name</h1>
            </th>
          </tr>
        </thead>
        <tbody>{renderCharactersList(results)}</tbody>
      </table>

      <div className="buttonWrapper">
        {next && <button onClick={() => nextPage(next)}>NEXT</button>}
      </div>
    </>
  );
};

export default List;
