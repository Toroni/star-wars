import React, { useState, useEffect } from "react";
import { getDataCharacter, getInfo } from "./characterInfoProvider";

const CharacterInfo = (props) => {
  const [data, setData] = useState(null);
  const [info, setInfo] = useState(null);

  const fetchData = async (url) => {
    const result = await getDataCharacter(url);
    setData(result);
  };
  useEffect(() => {
    fetchData(props.location.state);
  }, [props.location.state]);

  const getAllInformation = async ({ species, starships, films }) => {
    const prepareData = [
      { urls: species },
      { urls: starships },
      { urls: films, fieldName: "title" },
    ];

    const [speciesList, shipsList, filmsList] = await Promise.all(
      prepareData.map(({ urls, fieldName }) => getInfo(urls, fieldName))
    );
    
    setInfo({ shipsList, speciesList, filmsList });
  };

  useEffect(() => {
    if (data) {
      getAllInformation(data);
    }
  }, [data]);

  if (!data || !info) return null;

  const backToList = () => {
    props.history.push("/list");
  };

  return (
    <div>
      <table className="container">
        <thead>
          <tr>
            <th>
              <h1>Name</h1>
            </th>
            <th>
              <h1>Species</h1>
            </th>
            <th>
              <h1>Movies</h1>
            </th>
            <th>
              <h1>Spaceships</h1>
            </th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>{data.name}</td>
            <td>{info.speciesList}</td>
            <td>{info.filmsList}</td>
            <td>{info.shipsList}</td>
          </tr>
        </tbody>
      </table>
      <div className="buttonWrapper">
        <button onClick={() => backToList(data.next)}>BACK</button>
      </div>
    </div>
  );
};

export default CharacterInfo;
