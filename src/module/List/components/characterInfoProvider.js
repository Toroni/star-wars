export const getDataCharacter = async (url) => {
    try {
        const result = await fetch(url)
            .then((data) => data.json())
        return result
    } catch (e) {
        console.log("fetchData -> e", e);
    }
}


export const getInfo = async (value, fieldName = "name") => {
    const reducer = (fieldName) => {
        return (acc, el,) => {
            acc.push(
                fetch(el)
                    .then((res) => res.json())
                    .then((res) => res[fieldName])
            );
            return acc;
        };
    }
    const result = await Promise.all(value.reduce(reducer(fieldName), []))
    return result.join(', ')
}