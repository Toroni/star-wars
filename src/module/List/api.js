
import axios from "axios";
import url from "./url"

export const getPeople = () => {
    return axios.get(url.GET_PEOPLE);
}
