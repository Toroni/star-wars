import url from "./url"


export const getDataList = async (value = url.GET_PEOPLE) => {
    try {
        const result = await fetch(value)
            .then((data) => data.json())
        return result
    } catch (e) {
        console.log("fetchData -> e", e);
    }
}


export const getSearchData = async (value) => {
    try {
        const res = await fetch(
            `https://swapi.dev/api/films/?search=${value}`
        ).then((res) => res.json());
        console.log(res)

        if (!res.results.length) return [{ name: "no data" }]

        return res.results.reduce(async (acc, { characters }) => {
            const charactersList = await getSearchInfo(characters)
            return [...acc, ...charactersList]
        }, [])

    } catch (e) {
        console.log(e);
    }

}


export const getSearchInfo = async (value) => {
    const reducer = ((acc, el,) => {
        acc.push(
            fetch(el)
                .then((res) => res.json())
        );
        return acc;
    })

    const data = await Promise.all(value.reduce(reducer, []))
    return data
}