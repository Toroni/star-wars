
import List from "./module/List/List";
import Character from "./module/List/components/CharacterInfo"

const route = [
    { path: "/list", exact: true, name: "Start", component: List },
    { path: "/character", exact: true, name: "Start", component: Character },
];

export default route;
