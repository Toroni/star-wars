import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import routes from "./routes"
import './App.css';

function App() {

  const roteList = routes.map((route, index) => {
    return (route.component) ? (
      <Route
        key={index}
        path={route.path}
        exact={route.exact}
        name={route.name}
        render={props => (
          <route.component {...props} />
        )} />
    ) : (null);
  });
  return (
    <div className="App">
      <Switch>
        {roteList}

        <Route exact path="/">
          <Redirect to="/list" />
        </Route>
      </Switch>
    </div>
  );
}

export default App;
